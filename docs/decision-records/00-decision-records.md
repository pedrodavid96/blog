# Adopt Lightweight (Architectural) Decision Records

Record decisions made in the project in a simple and structured way.

Status: Adoption

## Decision

Decisions should be documented.

We'll "drop" the "Architectural" part of "Lightweight Architectural
 Decision Records" since it's a good option to record every decision.

Decisions to be written under `docs/decision-records` in Markdown
 using a file in the format of "NN-title-with-dashes.md" (#N may increase
 in the future).

### Additional Context

> They answer "why?" questions about design and justify why an option
>  is chosen.

> ADRs capture the decision at the time it’s being made.
>
> * They’re not for you, they’re for future you 
>
> * They’re not for you, they’re for your peers
>
> * They’re not for you, they’re for your future peers
>
in https://github.blog/2020-08-13-why-write-adrs/

> They don’t get outdated!
>
> They are capsules of knowledge that haven proven to be tremendously
>  helpful for project maintenance.
> 
> They help code reviews. They encapsulate in the versioned code the
>  rationale behind the choices you made.
>
in https://understandlegacycode.com/blog/earn-maintainers-esteem-with-adrs/

## Considered alternatives

* Keeping the "Architectural" part of "Lightweight Architectural
 Decision Records"
  
  Decided against in order to record all decisions.

* Using underscores in file names

  Following the format described in https://adr.github.io/madr/#example
  > The filenames are following the pattern NNNN-title-with-dashes.md
  >
  > ...
  >  * the title is stored using dashes and lowercase, because adr-tools
  >     also does that.

## Consequences

### Positive

* Project now contains a structured log of the decisions made

* Decisions explain their rationale and the alternatives considered,
   as well as the reason why those weren't ideal

* Help code reviews by embeding this rationale in the versioned code

* Decision records are "low maintenance" documentation, as in, the log
   is meant to be kept and, as such, seldomly updated.
  
  This doesn't mean a Decision Record can't be updated but only that 

### Negative

* We don't enforce the creation of decision records

* Markdown format is not enforced

* File name convention is not enforced

* Additionally, while keeping documentation in the code is a good idea
   to ensure it evolves with it, code editors often don't have
   spellchecking enabled.
   
  We should strive to only land code without spelling errors.
