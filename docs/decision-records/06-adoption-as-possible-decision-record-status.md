# Adoption as possible Decision Record Status

Status: Adoption

## Decision

Allow "Adoption" as a Decision Record status.

From this
 [presentation](https://resources.sei.cmu.edu/asset_files/Presentation/2017_017_001_497746.pdf)
 and many more resources, usually the status is one of the following:
 [Proposed | Accepted | Deprecated | Superseded].

While they cover most cases, we want to cover the case where the Decision
 has already been partially accepted but we're still working on
 "the edges".

This encourages the developers to create the Decision Record as soon as
 possible but should free them from the choice of "maintaining" it or
 cluttering the log with small "superseded" reiterations.

The most common use case is tackling some of the negative consequences
 that would require additional work.

## Considered Options

Simply using the "Accepted" status.

While both messages are not completely incompatible, almost forcing a
 developer to both didn't feel right.
In one hand Decision Records should be "low maintenance" (as in, you should
 seldomly update them, possibly only when superseded on in repository
 changes).
On the other hand, we would be 

## Consequences

### Positive

* Reduces friction to create documentation

* Encourages gradual innovation with the shortcomings under work documented

### Negative

* Current status is not enforced
