# Prefer dashes in filenames

Filenames could follow different standards (camelCase, snake_case, ...).

Status: Adoption

## Decision

Filenames should prefer dashes (`-`) to (`_`) and avoid spaces (` `).

Most of this project resources are to be served on the web.
Google recommends `-`:
* https://developers.google.com/style/filenames

  > Recommended: avoiding-cliches.jd
  >
  > Sometimes OK: avoiding_cliches.jd
  >
  > Not recommended: avoidingcliches.jd, avoidingCliches.jd, avoiding-clichés.jd

* https://developers.google.com/search/docs/advanced/guidelines/url-structure?hl=en&visit_id=637416078484042148-3488486073&rd=1

  > Consider using punctuation in your URLs. 
  > The URL http://www.example.com/green-dress.html is much more useful
     to us than http://www.example.com/greendress.html.
  > We recommend that you use hyphens (`-`) instead of underscores (`_`)
     in your URLs.

## Considered Options

* Preferring `_` 

## Consequences

### Positive

* Convention is now documented and easily accessible by anyone

### Negative

* Still not enforced
