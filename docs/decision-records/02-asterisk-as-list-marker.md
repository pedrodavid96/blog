# Use asterisk as list marker in Markdown documents

Lists in markdown can be indicated by `*` (asterisk) or `-` (hyphen)
 in Markdown documents.

Status: Adoption

## Decision

Use an asterisk.

An asterisk does not have a meaning of "good" or "bad", whereas a hyphen `-` could be read as indicator of something negative (in contrast to `+`, which could be more be read as "good").

According to the [Markdown Style Guide](http://www.cirosantilli.com/markdown-style-guide/), an asterisk as list marker is more readable (see [readability profile](http://www.cirosantilli.com/markdown-style-guide/#readability-profile)).

## Considered Options

* Use a hyphen

## Consequences

### Positive

* Improved Style Guide
* Hopefully improves consistency

### Negative

* Not enforced
