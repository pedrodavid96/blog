# Adopt editorconfig

https://editorconfig.org/

> EditorConfig helps maintain consistent coding styles for multiple
   developers working on the same project across various editors and
   IDEs.
>
In https://editorconfig-specification.readthedocs.io/en/latest/

Status: Adoption

## Decision

Adopt [`.editorconfig`](https://editorconfig.org/) as a best effort to
 use the same settings across different editors.

_Note:_ **This is not an enforcement**, it simply **helps** maintaining
 a consistent coding style.

## Considered alternatives

* editor specific settings

  Wouldn't work across different editors (e.g.: vim, visual code, IDEA...)

## Consequences

### Positive

* Most editors should be able to automatically format the code according to
   configuration.

### Negative

* Having a .editorconfig file helps but doesn't enforce a certain style.
  
  That should be achieved by a different tool,
   e.g.: [prettier](https://prettier.io/)
  When adopting such tool we should check if .editorconfig can be used as
   a source for base configurations, otherwise linter / auto-formatter
   configurations might be different than what your editor.

* Editor / IDE may require an external plugin

* Editor / IDE may not be able to enforce all `.editorconfig` settings

  For example, as of writing this, visual code doesn't support
   `max_line_length`.
  [issue](https://github.com/editorconfig/editorconfig-vscode/issues/272)

* Editor / IDE may not support `.editorconfig` at all (rare)
