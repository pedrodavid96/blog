# Decision Records in Markdown format

Decision Records format should be established.

Status: Adopting

## Decision

Record Decisions in [Markdown format](https://daringfireball.net/projects/markdown/).

The decision records must be stored in a plain text format:

* This works well with version control systems.

* It allows the tool to modify the status of records and insert
  hyperlinks when one decision supersedes another.

* Decisions can be read in the terminal, IDE, version control
  browser, etc.

People will want to use some formatting: lists, code examples,
and so on.

People will want to view the decision records in a more readable
format than plain text, and maybe print them out.

## Considered alternatives

None in particular.

A format with additional metadata could come handy, for example, to link
 to another document from title only (see the "portability argument" in
 [No number in decision record](02-no-number-in-decision-record-heading.md)).
Although additional metadata would have it's cons:

* Possibly more clutter in the source, becoming harder to read without a
   renderer

* Possibly more work by the writer, further demotivating from
   documenting, the complete opposite from our goal.

## Consequences

### Positive

* Agreed and documented the format in which Decision Records are
   expected to be written in.

* Decisions can be read in the terminal.

* Decisions will be formatted nicely and hyperlinked by the
   browsers of project hosting sites like GitHub and Bitbucket.

* Tools like [pandoc](http://pandoc.org/) can be used to convert
   the decision records into HTML or PDF.

### Negative

* TODO: Different flavors of Markdown to choose from

* TODO: No linters

* TODO: No documented standard over options that are too flexible (e.g.:
   list markers could be any of `-`, `*`, `+` ...)
