# Do not use numbers in headings

While it is useful to have the Decision Record number available, that
 information is already on the file name and any renderer should be able
 to use it if needed.

Status: Adoption

## Decision

Use the title only.

* This is common in other markdown files, too.
  One does not add numbering manually at the markdown files, but tries to get the numbers injected by the rendering framework or CSS.

## Considered Options

* Add the ADR number in front of the title (e.g., "# 2. Do not use numbers in headings")

  Additional work that would not be enforced and that would only increase effort both during
   development (e.g.: after rebase) and when trying to reuse the Decision Record in a different

## Consequences

### Positive

* Enables renaming of ADRs (before publication) easily

* Allows copy'n'paste of ADRs from other repositories without having to worry about the numbers.

### Negative

* Hard to identify the ADR by the title alone

  This should only affect the "source" since the rendered Markdown can have the number as well.
