install:

.PHONY: lint
lint:
	markdownlint-cli2 "**/*.md" "#node_modules"

.PHONY: decision-record
decision-record: | .decision-records
	@cp ./docs/decision-records/template.md .decision-records/
	@$${EDITOR:-vi} .decision-records/template.md
	@mv .decision-records/template.md ./docs/decision-records/new.md

.decision-records:
	@mkdir -p .decision-records/
