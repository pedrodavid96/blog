#!/usr/bin/env bash

mkdir -p .adr/
cp ./docs/decision-records/template.md .adr/
${EDITOR:-vi} .adr/template.md
mv .adr/template.md ./docs/decision-records/new.md
